/*-----------------------------------------------------------------------

  Source: src/snes/tutorials/ex19.c

  We thank David E. Keyes for contributing the driven cavity discretization within this example code.

  This problem is modeled by the partial differential equation system

  \begin{eqnarray}
    - \triangle U - \nabla_y \Omega & = & 0  \\
    - \triangle V + \nabla_x\Omega & = & 0  \\
    - \triangle \Omega + \nabla \cdot ([U*\Omega,V*\Omega]) - GR* \nabla_x T & = & 0  \\
    - \triangle T + PR* \nabla \cdot ([U*T,V*T]) & = & 0
  \end{eqnarray}

  in the unit square, which is uniformly discretized in each of x and y in this simple encoding.

  No-slip, rigid-wall Dirichlet conditions are used for $ [U,V]$.
  Dirichlet conditions are used for Omega, based on the definition of
  vorticity: $ \Omega = - \nabla_y U + \nabla_x V$, where along each
  constant coordinate boundary, the tangential derivative is zero.
  Dirichlet conditions are used for T on the left and right walls,
  and insulation homogeneous Neumann conditions are used for T on
  the top and bottom walls.

  A finite difference approximation with the usual 5-point stencil
  is used to discretize the boundary value problem to obtain a
  nonlinear system of equations.  Upwinding is used for the divergence
  (convective) terms and central for the gradient (source) terms.

  The Jacobian can be either formed via finite differencing using coloring

  ------------------------------------------------------------------------*/

#include <petscdm.h>
#include <petscdmda.h>
#include <petscsnes.h>
#include "dyn_petsc.h"

// scons example=DynPETScSNE compileMode=release > /dev/null && prterun -n 2 --host n01:2,n02:2,n03:2,n04:2 ./build/DynPETScSNE_release
// scons example=DynPETScSNE compileMode=release > /dev/null && prterun -n 4 --host n01:2,n02:2,n03:2,n04:2 ./build/DynPETScSNE_release -da_refine 2 -grashof 1.4e4 -pc_type mg -npc_snes_type nrichardson -npc_snes_max_it 1 -da_grid_x 8 -da_grid_y 8
// scons example=DynPETScSNE compileMode=release > /dev/null && prterun -n 2 --host n01:2,n02:2,n03:2,n04:2 ./build/DynPETScSNE_release -ksp_type bcgs -grashof 1.3e4 -da_refine 4 -da_grid_x 4 -da_grid_y 4 -pc_type mg
// prterun -n 4 --host $HOSTS ./build/DynPETScSNE_release -da_refine 4 -grashof 1.4e4 -pc_type mg -npc_snes_type nrichardson -npc_snes_max_it 1 -snes_max_it 1000 -iters_per_step 20
// prterun -n 4 --host $HOSTS ./build/DynPETScSNE_release  -grashof 1.4e4 -pc_type mg -npc_snes_type nrichardson -snes_max_it 1000 -iters_per_step 20 -da_grid_x 64 -da_grid_y 64 -da_refine 1
// prterun -n 112 --host $HOSTS ./build/DynPETScSNE_release -grashof 2.5e1 -snes_max_it 1000 -iters_per_step 50 -da_grid_x 2048 -da_grid_y 2048 -snes_type anderson

typedef struct {
  PetscScalar u, v, omega, temp;
} Field;

typedef struct {
  PetscReal lidvelocity, prandtl, grashof; /* physical parameters */
  PetscBool draw_contours;                 /* flag - 1 indicates drawing contours */
} AppCtx;

/* User-defined routines. */
PetscErrorCode FormFunctionLocal(DMDALocalInfo *, Field **, Field **, void *);
PetscErrorCode FormInitialGuess(AppCtx *, DM, Vec);
PetscErrorCode NonlinearGS(SNES, Vec, Vec, void *);
PetscErrorCode Monitor();
PetscErrorCode SetInitialConditions();
PetscErrorCode InitDM();
PetscErrorCode InitSNE();

static SNES snes;
static Vec x;
static DM da;
static AppCtx   user;              /* user-defined work context */
static PetscInt mx, my, its;
static SNESConvergedReason reason;
static PetscInt iters_per_step = 3; // Max iterations for each step

static PetscInt max_iters = 1000; // Maximum number of total iterations
static PetscInt curr_step = 0;
static bool initialized_dm = false;

int main(int argc, char **argv) {
   PetscFunctionBeginUser;

   PetscCall(InitPetsc(&argc, &argv));

   PetscCall(PetscOptionsGetInt(NULL, NULL, "-max_iters", &max_iters, NULL));
   PetscCall(PetscOptionsGetInt(NULL, NULL, "-iters_per_step", &iters_per_step, NULL));

   PetscCall(InitDM());
   PetscCall(InitSNE());

   PetscCall(PetscOptionsGetReal(NULL, NULL, "-lidvelocity", &user.lidvelocity, NULL));
   PetscCall(PetscOptionsGetReal(NULL, NULL, "-prandtl", &user.prandtl, NULL));
   PetscCall(PetscOptionsGetReal(NULL, NULL, "-grashof", &user.grashof, NULL));
   PetscCall(PetscOptionsHasName(NULL, NULL, "-contours", &user.draw_contours));

   if (!is_dynamic) {
      PetscCall(SetInitialConditions());
   }

   // Start timer before calculating
   if (world_rank == 0) {
      PetscCall(PetscTime(&t_start_global));
   }

   while(max_iters > 0) {

      PetscCall(Step());

      PetscCall(Monitor());

      // Check if converged, then stop
      if (reason > 0) break;

      max_iters -= its; // decrease maximum iterations
      ++curr_step;

      int pset_op_fail = SendPsetOp();

      if (pset_op_fail) {
         PetscCall(PrintNewLine());
         continue;
      }

      if (terminate) goto clean_up;
      
      PetscCall(RedistributeObjects());

      PetscCall(AdvancedMonitor());
   }

   PetscCall(PrintResults());

clean_up:
   PetscCall(CleanUp());

   PetscFunctionReturn(PETSC_SUCCESS);
}

PetscErrorCode Step() {
   PetscFunctionBeginUser;

   // measure elapsed time for each iteration
   PetscCall(PetscTime(&t_start));

   PetscCall(SNESSolve(snes, NULL, x));

   PetscCall(PetscTime(&t_end));

   PetscLogDouble elapsed = t_end - t_start;
   PetscCallMPI(MPI_Reduce(&elapsed, &t_step, 1, MPI_DOUBLE, MPI_MAX, 0, comm_world));

   t_step_sum += t_step;

   PetscCall(SNESGetIterationNumber(snes, &its));
   PetscCall(SNESGetConvergedReason(snes, &reason));

   PetscFunctionReturn(PETSC_SUCCESS);
}

PetscErrorCode SetInitialConditions() {
   PetscFunctionBeginUser;
   PetscCall(FormInitialGuess(&user, da, x));
   PetscFunctionReturn(PETSC_SUCCESS);
}

PetscErrorCode Monitor() {
   PetscScalar error_norm;

   PetscFunctionBeginUser;

   if (disable_monitor) PetscFunctionReturn(PETSC_SUCCESS);

   VecNorm(x, NORM_2, &error_norm);


   PetscCall(PetscPrintf(comm_world, "Size: %4d, Step: %3d, Error: %12g, Its: %2" PetscInt_FMT ", Elapsed: %5f seconds, Conv: %s",
                                                world_size, curr_step, error_norm, its, t_end - t_start, SNESConvergedReasons[reason]));

   if (!advanced_monitor) PetscCall(PetscPrintf(comm_world, "\n"));

   PetscFunctionReturn(PETSC_SUCCESS);
}

PetscErrorCode CleanUp() {
  PetscFunctionBeginUser;

  if (terminate) {
    PetscCall(VecCopyDifferentComm(x, 0, NULL, 0));
  }

  PetscCall(SNESDestroy(&snes));
  PetscCall(DMDestroy(&da));
  PetscCall(VecDestroy(&x));

   PetscCallMPI(MPI_Comm_disconnect(&comm_world));

   PETSC_COMM_WORLD = PETSC_COMM_SELF;
   PetscCall(PetscFinalize());

  PetscFunctionReturn(PETSC_SUCCESS);
}

PetscErrorCode InitSNE() {
   PetscFunctionBeginUser;

   /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Create nonlinear solver context
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

   PetscCall(SNESCreate(comm_world, &snes));
   PetscCall(SNESSetDM(snes, (DM)da));
   PetscCall(SNESSetNGS(snes, NonlinearGS, (void *)&user));

   PetscCall(DMDASNESSetFunctionLocal(da, INSERT_VALUES, (PetscErrorCode(*)(DMDALocalInfo *, void *, void *, void *))FormFunctionLocal, &user));
   PetscCall(SNESSetFromOptions(snes));

   PetscCall(SNESSetTolerances(snes, PETSC_DEFAULT, PETSC_DEFAULT, PETSC_DEFAULT, iters_per_step, PETSC_DEFAULT));

   if (is_dynamic || initialized_dm) {
      PetscCallMPI(MPI_Bcast(&max_iters, 1, MPI_INT, 0, comm_world));
      PetscCallMPI(MPI_Bcast(&curr_step, 1, MPI_DOUBLE, 0, comm_world));
   }

   PetscFunctionReturn(PETSC_SUCCESS);
}

PetscErrorCode InitDM() {
   Vec _x = x;
   PetscFunctionBeginUser;

   /*
         Create distributed array object to manage parallel grid and vectors
         for principal unknowns (x) and governing residuals (f)
   */
   PetscCall(DMDACreate2d(comm_world, DM_BOUNDARY_NONE, DM_BOUNDARY_NONE, DMDA_STENCIL_STAR, 4, 4, PETSC_DECIDE, PETSC_DECIDE, 4, 1, 0, 0, &da));
   PetscCall(DMSetFromOptions(da));
   PetscCall(DMSetUp(da));

   PetscCall(DMDAGetInfo(da, 0, &mx, &my, PETSC_IGNORE, PETSC_IGNORE, PETSC_IGNORE, PETSC_IGNORE, PETSC_IGNORE, PETSC_IGNORE, PETSC_IGNORE, PETSC_IGNORE, PETSC_IGNORE, PETSC_IGNORE));
   /*
      Problem parameters (velocity of lid, prandtl, and grashof numbers)
   */
   user.lidvelocity = 1.0 / (mx * my);
   user.prandtl     = 1.0;
   user.grashof     = 1.0;

   PetscCall(DMDASetFieldName(da, 0, "x_velocity"));
   PetscCall(DMDASetFieldName(da, 1, "y_velocity"));
   PetscCall(DMDASetFieldName(da, 2, "Omega"));
   PetscCall(DMDASetFieldName(da, 3, "temperature"));

   PetscCall(DMSetApplicationContext(da, &user));

   PetscCall(DMCreateGlobalVector(da, &x));
   /*
      Dynamically allocated processes should copy solution vector 
   */
   int should_copy = is_dynamic || initialized_dm;
   if (should_copy) PetscCall(VecCopyDifferentComm(_x, 0, x, 0));

   if (_x) PetscCall(VecDestroy(&_x));

   // PetscCall(VecDuplicate(x, &r));

   /*
      Set names for some vectors to facilitate monitoring (optional)
   */
   PetscCall(PetscObjectSetName((PetscObject) x, "Approximate Solution"));

   initialized_dm = true;

   PetscFunctionReturn(PETSC_SUCCESS);
}

PetscErrorCode RedistributeObjects() {
   MPI_Comm old_comm = PETSC_COMM_WORLD;

   PetscFunctionBeginUser;

   if (world_rank == 0) PetscCall(PetscTime(&t_start));

   /*
      Destroy old objects
   */
   PetscCall(SNESDestroy(&snes));
   PetscCall(DMDestroy(&da));

   PetscCall(SetCommFromSession());

   if (next_op == MPI_PSETOP_GROW) PETSC_COMM_WORLD = comm_world;

   PetscCall(InitDM());
   PetscCall(InitSNE());

   if (next_op == MPI_PSETOP_SHRINK) PETSC_COMM_WORLD = comm_world;

   if (world_rank == 0) {
      PetscCall(PetscTime(&t_end));
      t_redis = t_end - t_start;
      t_redis_sum += t_redis;
   }

   PetscCallMPI(MPI_Comm_disconnect(&old_comm));

   PetscFunctionReturn(PETSC_SUCCESS);
}

PetscErrorCode FormInitialGuess(AppCtx *user, DM da, Vec X) {
  PetscInt  i, j, mx, xs, ys, xm, ym;
  PetscReal grashof, dx;
  Field   **x;

  PetscFunctionBeginUser;
  grashof = user->grashof;

  PetscCall(DMDAGetInfo(da, 0, &mx, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0));
  dx = 1.0 / (mx - 1);

  /*
     Get local grid boundaries (for 2-dimensional DMDA):
       xs, ys   - starting grid indices (no ghost points)
       xm, ym   - widths of local grid (no ghost points)
  */
  PetscCall(DMDAGetCorners(da, &xs, &ys, NULL, &xm, &ym, NULL));

  /*
     Get a pointer to vector data.
       - For default PETSc vectors, VecGetArray() returns a pointer to
         the data array.  Otherwise, the routine is implementation dependent.
       - You MUST call VecRestoreArray() when you no longer need access to
         the array.
  */
  PetscCall(DMDAVecGetArrayWrite(da, X, &x));

  /*
     Compute initial guess over the locally owned part of the grid
     Initial condition is motionless fluid and equilibrium temperature
  */
  for (j = ys; j < ys + ym; j++) {
    for (i = xs; i < xs + xm; i++) {
      x[j][i].u     = 0.0;
      x[j][i].v     = 0.0;
      x[j][i].omega = 0.0;
      x[j][i].temp  = (grashof > 0) * i * dx;
    }
  }

  /*
     Restore vector
  */
  PetscCall(DMDAVecRestoreArrayWrite(da, X, &x));
  PetscFunctionReturn(PETSC_SUCCESS);
}

PetscErrorCode FormFunctionLocal(DMDALocalInfo *info, Field **x, Field **f, void *ptr) {
  AppCtx     *user = (AppCtx *)ptr;
  PetscInt    xints, xinte, yints, yinte, i, j;
  PetscReal   hx, hy, dhx, dhy, hxdhy, hydhx;
  PetscReal   grashof, prandtl, lid;
  PetscScalar u, uxx, uyy, vx, vy, avx, avy, vxp, vxm, vyp, vym;

  PetscFunctionBeginUser;
  grashof = user->grashof;
  prandtl = user->prandtl;
  lid     = user->lidvelocity;

  /*
     Define mesh intervals ratios for uniform grid.

     Note: FD formulae below are normalized by multiplying through by
     local volume element (i.e. hx*hy) to obtain coefficients O(1) in two dimensions.

  */
  dhx   = (PetscReal)(info->mx - 1);
  dhy   = (PetscReal)(info->my - 1);
  hx    = 1.0 / dhx;
  hy    = 1.0 / dhy;
  hxdhy = hx * dhy;
  hydhx = hy * dhx;

  xints = info->xs;
  xinte = info->xs + info->xm;
  yints = info->ys;
  yinte = info->ys + info->ym;

  /* Test whether we are on the bottom edge of the global array */
  if (yints == 0) {
    j     = 0;
    yints = yints + 1;
    /* bottom edge */
    for (i = info->xs; i < info->xs + info->xm; i++) {
      f[j][i].u     = x[j][i].u;
      f[j][i].v     = x[j][i].v;
      f[j][i].omega = x[j][i].omega + (x[j + 1][i].u - x[j][i].u) * dhy;
      f[j][i].temp  = x[j][i].temp - x[j + 1][i].temp;
    }
  }

  /* Test whether we are on the top edge of the global array */
  if (yinte == info->my) {
    j     = info->my - 1;
    yinte = yinte - 1;
    /* top edge */
    for (i = info->xs; i < info->xs + info->xm; i++) {
      f[j][i].u     = x[j][i].u - lid;
      f[j][i].v     = x[j][i].v;
      f[j][i].omega = x[j][i].omega + (x[j][i].u - x[j - 1][i].u) * dhy;
      f[j][i].temp  = x[j][i].temp - x[j - 1][i].temp;
    }
  }

  /* Test whether we are on the left edge of the global array */
  if (xints == 0) {
    i     = 0;
    xints = xints + 1;
    /* left edge */
    for (j = info->ys; j < info->ys + info->ym; j++) {
      f[j][i].u     = x[j][i].u;
      f[j][i].v     = x[j][i].v;
      f[j][i].omega = x[j][i].omega - (x[j][i + 1].v - x[j][i].v) * dhx;
      f[j][i].temp  = x[j][i].temp;
    }
  }

  /* Test whether we are on the right edge of the global array */
  if (xinte == info->mx) {
    i     = info->mx - 1;
    xinte = xinte - 1;
    /* right edge */
    for (j = info->ys; j < info->ys + info->ym; j++) {
      f[j][i].u     = x[j][i].u;
      f[j][i].v     = x[j][i].v;
      f[j][i].omega = x[j][i].omega - (x[j][i].v - x[j][i - 1].v) * dhx;
      f[j][i].temp  = x[j][i].temp - (PetscReal)(grashof > 0);
    }
  }

  /* Compute over the interior points */
  for (j = yints; j < yinte; j++) {
    for (i = xints; i < xinte; i++) {
      /*
       convective coefficients for upwinding
      */
      vx  = x[j][i].u;
      avx = PetscAbsScalar(vx);
      vxp = .5 * (vx + avx);
      vxm = .5 * (vx - avx);
      vy  = x[j][i].v;
      avy = PetscAbsScalar(vy);
      vyp = .5 * (vy + avy);
      vym = .5 * (vy - avy);

      /* U velocity */
      u         = x[j][i].u;
      uxx       = (2.0 * u - x[j][i - 1].u - x[j][i + 1].u) * hydhx;
      uyy       = (2.0 * u - x[j - 1][i].u - x[j + 1][i].u) * hxdhy;
      f[j][i].u = uxx + uyy - .5 * (x[j + 1][i].omega - x[j - 1][i].omega) * hx;

      /* V velocity */
      u         = x[j][i].v;
      uxx       = (2.0 * u - x[j][i - 1].v - x[j][i + 1].v) * hydhx;
      uyy       = (2.0 * u - x[j - 1][i].v - x[j + 1][i].v) * hxdhy;
      f[j][i].v = uxx + uyy + .5 * (x[j][i + 1].omega - x[j][i - 1].omega) * hy;

      /* Omega */
      u             = x[j][i].omega;
      uxx           = (2.0 * u - x[j][i - 1].omega - x[j][i + 1].omega) * hydhx;
      uyy           = (2.0 * u - x[j - 1][i].omega - x[j + 1][i].omega) * hxdhy;
      f[j][i].omega = uxx + uyy + (vxp * (u - x[j][i - 1].omega) + vxm * (x[j][i + 1].omega - u)) * hy + (vyp * (u - x[j - 1][i].omega) + vym * (x[j + 1][i].omega - u)) * hx - .5 * grashof * (x[j][i + 1].temp - x[j][i - 1].temp) * hy;

      /* Temperature */
      u            = x[j][i].temp;
      uxx          = (2.0 * u - x[j][i - 1].temp - x[j][i + 1].temp) * hydhx;
      uyy          = (2.0 * u - x[j - 1][i].temp - x[j + 1][i].temp) * hxdhy;
      f[j][i].temp = uxx + uyy + prandtl * ((vxp * (u - x[j][i - 1].temp) + vxm * (x[j][i + 1].temp - u)) * hy + (vyp * (u - x[j - 1][i].temp) + vym * (x[j + 1][i].temp - u)) * hx);
    }
  }

  /*
     Flop count (multiply-adds are counted as 2 operations)
  */
  PetscCall(PetscLogFlops(84.0 * info->ym * info->xm));
  PetscFunctionReturn(PETSC_SUCCESS);
}

PetscErrorCode NonlinearGS(SNES snes, Vec X, Vec B, void *ctx) {
  DMDALocalInfo info;
  Field       **x, **b;
  Vec           localX, localB;
  DM            da;
  PetscInt      xints, xinte, yints, yinte, i, j, k, l;
  PetscInt      max_its, tot_its;
  PetscInt      sweeps;
  PetscReal     rtol, atol, stol;
  PetscReal     hx, hy, dhx, dhy, hxdhy, hydhx;
  PetscReal     grashof, prandtl, lid;
  PetscScalar   u, uxx, uyy, vx, vy, avx, avy, vxp, vxm, vyp, vym;
  PetscScalar   fu, fv, fomega, ftemp;
  PetscScalar   dfudu;
  PetscScalar   dfvdv;
  PetscScalar   dfodu, dfodv, dfodo;
  PetscScalar   dftdu, dftdv, dftdt;
  PetscScalar   yu = 0, yv = 0, yo = 0, yt = 0;
  PetscScalar   bjiu, bjiv, bjiomega, bjitemp;
  PetscBool     ptconverged;
  PetscReal     pfnorm, pfnorm0, pynorm, pxnorm;
  AppCtx       *user = (AppCtx *)ctx;

  PetscFunctionBeginUser;
  grashof = user->grashof;
  prandtl = user->prandtl;
  lid     = user->lidvelocity;
  tot_its = 0;
  PetscCall(SNESNGSGetTolerances(snes, &rtol, &atol, &stol, &max_its));
  PetscCall(SNESNGSGetSweeps(snes, &sweeps));
  PetscCall(SNESGetDM(snes, (DM *)&da));
  PetscCall(DMGetLocalVector(da, &localX));
  if (B) PetscCall(DMGetLocalVector(da, &localB));
  /*
     Scatter ghost points to local vector, using the 2-step process
        DMGlobalToLocalBegin(), DMGlobalToLocalEnd().
  */
  PetscCall(DMGlobalToLocalBegin(da, X, INSERT_VALUES, localX));
  PetscCall(DMGlobalToLocalEnd(da, X, INSERT_VALUES, localX));
  if (B) {
    PetscCall(DMGlobalToLocalBegin(da, B, INSERT_VALUES, localB));
    PetscCall(DMGlobalToLocalEnd(da, B, INSERT_VALUES, localB));
  }
  PetscCall(DMDAGetLocalInfo(da, &info));
  PetscCall(DMDAVecGetArrayWrite(da, localX, &x));
  if (B) PetscCall(DMDAVecGetArrayRead(da, localB, &b));
  /* looks like a combination of the formfunction / formjacobian routines */
  dhx   = (PetscReal)(info.mx - 1);
  dhy   = (PetscReal)(info.my - 1);
  hx    = 1.0 / dhx;
  hy    = 1.0 / dhy;
  hxdhy = hx * dhy;
  hydhx = hy * dhx;

  xints = info.xs;
  xinte = info.xs + info.xm;
  yints = info.ys;
  yinte = info.ys + info.ym;

  /* Set the boundary conditions on the momentum equations */
  /* Test whether we are on the bottom edge of the global array */
  if (yints == 0) {
    j = 0;
    /* bottom edge */
    for (i = info.xs; i < info.xs + info.xm; i++) {
      if (B) {
        bjiu = b[j][i].u;
        bjiv = b[j][i].v;
      } else {
        bjiu = 0.0;
        bjiv = 0.0;
      }
      x[j][i].u = 0.0 + bjiu;
      x[j][i].v = 0.0 + bjiv;
    }
  }

  /* Test whether we are on the top edge of the global array */
  if (yinte == info.my) {
    j = info.my - 1;
    /* top edge */
    for (i = info.xs; i < info.xs + info.xm; i++) {
      if (B) {
        bjiu = b[j][i].u;
        bjiv = b[j][i].v;
      } else {
        bjiu = 0.0;
        bjiv = 0.0;
      }
      x[j][i].u = lid + bjiu;
      x[j][i].v = bjiv;
    }
  }

  /* Test whether we are on the left edge of the global array */
  if (xints == 0) {
    i = 0;
    /* left edge */
    for (j = info.ys; j < info.ys + info.ym; j++) {
      if (B) {
        bjiu = b[j][i].u;
        bjiv = b[j][i].v;
      } else {
        bjiu = 0.0;
        bjiv = 0.0;
      }
      x[j][i].u = 0.0 + bjiu;
      x[j][i].v = 0.0 + bjiv;
    }
  }

  /* Test whether we are on the right edge of the global array */
  if (xinte == info.mx) {
    i = info.mx - 1;
    /* right edge */
    for (j = info.ys; j < info.ys + info.ym; j++) {
      if (B) {
        bjiu = b[j][i].u;
        bjiv = b[j][i].v;
      } else {
        bjiu = 0.0;
        bjiv = 0.0;
      }
      x[j][i].u = 0.0 + bjiu;
      x[j][i].v = 0.0 + bjiv;
    }
  }

  for (k = 0; k < sweeps; k++) {
    for (j = info.ys; j < info.ys + info.ym; j++) {
      for (i = info.xs; i < info.xs + info.xm; i++) {
        ptconverged = PETSC_FALSE;
        pfnorm0     = 0.0;
        fu          = 0.0;
        fv          = 0.0;
        fomega      = 0.0;
        ftemp       = 0.0;
        /*  Run Newton's method on a single grid point */
        for (l = 0; l < max_its && !ptconverged; l++) {
          if (B) {
            bjiu     = b[j][i].u;
            bjiv     = b[j][i].v;
            bjiomega = b[j][i].omega;
            bjitemp  = b[j][i].temp;
          } else {
            bjiu     = 0.0;
            bjiv     = 0.0;
            bjiomega = 0.0;
            bjitemp  = 0.0;
          }

          if (i != 0 && i != info.mx - 1 && j != 0 && j != info.my - 1) {
            /* U velocity */
            u     = x[j][i].u;
            uxx   = (2.0 * u - x[j][i - 1].u - x[j][i + 1].u) * hydhx;
            uyy   = (2.0 * u - x[j - 1][i].u - x[j + 1][i].u) * hxdhy;
            fu    = uxx + uyy - .5 * (x[j + 1][i].omega - x[j - 1][i].omega) * hx - bjiu;
            dfudu = 2.0 * (hydhx + hxdhy);
            /* V velocity */
            u     = x[j][i].v;
            uxx   = (2.0 * u - x[j][i - 1].v - x[j][i + 1].v) * hydhx;
            uyy   = (2.0 * u - x[j - 1][i].v - x[j + 1][i].v) * hxdhy;
            fv    = uxx + uyy + .5 * (x[j][i + 1].omega - x[j][i - 1].omega) * hy - bjiv;
            dfvdv = 2.0 * (hydhx + hxdhy);
            /*
             convective coefficients for upwinding
             */
            vx  = x[j][i].u;
            avx = PetscAbsScalar(vx);
            vxp = .5 * (vx + avx);
            vxm = .5 * (vx - avx);
            vy  = x[j][i].v;
            avy = PetscAbsScalar(vy);
            vyp = .5 * (vy + avy);
            vym = .5 * (vy - avy);
            /* Omega */
            u      = x[j][i].omega;
            uxx    = (2.0 * u - x[j][i - 1].omega - x[j][i + 1].omega) * hydhx;
            uyy    = (2.0 * u - x[j - 1][i].omega - x[j + 1][i].omega) * hxdhy;
            fomega = uxx + uyy + (vxp * (u - x[j][i - 1].omega) + vxm * (x[j][i + 1].omega - u)) * hy + (vyp * (u - x[j - 1][i].omega) + vym * (x[j + 1][i].omega - u)) * hx - .5 * grashof * (x[j][i + 1].temp - x[j][i - 1].temp) * hy - bjiomega;
            /* convective coefficient derivatives */
            dfodo = 2.0 * (hydhx + hxdhy) + ((vxp - vxm) * hy + (vyp - vym) * hx);
            if (PetscRealPart(vx) > 0.0) dfodu = (u - x[j][i - 1].omega) * hy;
            else dfodu = (x[j][i + 1].omega - u) * hy;

            if (PetscRealPart(vy) > 0.0) dfodv = (u - x[j - 1][i].omega) * hx;
            else dfodv = (x[j + 1][i].omega - u) * hx;

            /* Temperature */
            u     = x[j][i].temp;
            uxx   = (2.0 * u - x[j][i - 1].temp - x[j][i + 1].temp) * hydhx;
            uyy   = (2.0 * u - x[j - 1][i].temp - x[j + 1][i].temp) * hxdhy;
            ftemp = uxx + uyy + prandtl * ((vxp * (u - x[j][i - 1].temp) + vxm * (x[j][i + 1].temp - u)) * hy + (vyp * (u - x[j - 1][i].temp) + vym * (x[j + 1][i].temp - u)) * hx) - bjitemp;
            dftdt = 2.0 * (hydhx + hxdhy) + prandtl * ((vxp - vxm) * hy + (vyp - vym) * hx);
            if (PetscRealPart(vx) > 0.0) dftdu = prandtl * (u - x[j][i - 1].temp) * hy;
            else dftdu = prandtl * (x[j][i + 1].temp - u) * hy;

            if (PetscRealPart(vy) > 0.0) dftdv = prandtl * (u - x[j - 1][i].temp) * hx;
            else dftdv = prandtl * (x[j + 1][i].temp - u) * hx;

            /* invert the system:
             [ dfu / du     0        0        0    ][yu] = [fu]
             [     0    dfv / dv     0        0    ][yv]   [fv]
             [ dfo / du dfo / dv dfo / do     0    ][yo]   [fo]
             [ dft / du dft / dv     0    dft / dt ][yt]   [ft]
             by simple back-substitution
           */
            yu = fu / dfudu;
            yv = fv / dfvdv;
            yo = (fomega - (dfodu * yu + dfodv * yv)) / dfodo;
            yt = (ftemp - (dftdu * yu + dftdv * yv)) / dftdt;

            x[j][i].u     = x[j][i].u - yu;
            x[j][i].v     = x[j][i].v - yv;
            x[j][i].temp  = x[j][i].temp - yt;
            x[j][i].omega = x[j][i].omega - yo;
          }
          if (i == 0) {
            fomega        = x[j][i].omega - (x[j][i + 1].v - x[j][i].v) * dhx - bjiomega;
            ftemp         = x[j][i].temp - bjitemp;
            yo            = fomega;
            yt            = ftemp;
            x[j][i].omega = x[j][i].omega - fomega;
            x[j][i].temp  = x[j][i].temp - ftemp;
          }
          if (i == info.mx - 1) {
            fomega        = x[j][i].omega - (x[j][i].v - x[j][i - 1].v) * dhx - bjiomega;
            ftemp         = x[j][i].temp - (PetscReal)(grashof > 0) - bjitemp;
            yo            = fomega;
            yt            = ftemp;
            x[j][i].omega = x[j][i].omega - fomega;
            x[j][i].temp  = x[j][i].temp - ftemp;
          }
          if (j == 0) {
            fomega        = x[j][i].omega + (x[j + 1][i].u - x[j][i].u) * dhy - bjiomega;
            ftemp         = x[j][i].temp - x[j + 1][i].temp - bjitemp;
            yo            = fomega;
            yt            = ftemp;
            x[j][i].omega = x[j][i].omega - fomega;
            x[j][i].temp  = x[j][i].temp - ftemp;
          }
          if (j == info.my - 1) {
            fomega        = x[j][i].omega + (x[j][i].u - x[j - 1][i].u) * dhy - bjiomega;
            ftemp         = x[j][i].temp - x[j - 1][i].temp - bjitemp;
            yo            = fomega;
            yt            = ftemp;
            x[j][i].omega = x[j][i].omega - fomega;
            x[j][i].temp  = x[j][i].temp - ftemp;
          }
          tot_its++;
          pfnorm = PetscRealPart(fu * fu + fv * fv + fomega * fomega + ftemp * ftemp);
          pfnorm = PetscSqrtReal(pfnorm);
          pynorm = PetscRealPart(yu * yu + yv * yv + yo * yo + yt * yt);
          pynorm = PetscSqrtReal(pynorm);
          pxnorm = PetscRealPart(x[j][i].u * x[j][i].u + x[j][i].v * x[j][i].v + x[j][i].omega * x[j][i].omega + x[j][i].temp * x[j][i].temp);
          pxnorm = PetscSqrtReal(pxnorm);
          if (l == 0) pfnorm0 = pfnorm;
          if (rtol * pfnorm0 > pfnorm || atol > pfnorm || pxnorm * stol > pynorm) ptconverged = PETSC_TRUE;
        }
      }
    }
  }
  PetscCall(DMDAVecRestoreArrayWrite(da, localX, &x));
  if (B) PetscCall(DMDAVecRestoreArrayRead(da, localB, &b));
  PetscCall(DMLocalToGlobalBegin(da, localX, INSERT_VALUES, X));
  PetscCall(DMLocalToGlobalEnd(da, localX, INSERT_VALUES, X));
  PetscCall(PetscLogFlops(tot_its * (84.0 + 41.0 + 26.0)));
  PetscCall(DMRestoreLocalVector(da, &localX));
  if (B) PetscCall(DMRestoreLocalVector(da, &localB));
  PetscFunctionReturn(PETSC_SUCCESS);
}
