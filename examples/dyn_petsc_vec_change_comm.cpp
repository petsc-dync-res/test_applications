#include <petscvec.h>

// scons example=DynPETScVecChangeComm compileMode=release > /dev/null && prterun -n 8 --host n01:8 ./build/DynPETScVecChangeComm_release

int main(int argc, char **argv) {
  PetscInt N = 17;
  PetscScalar one = 1.0;
  Vec x = NULL, y = NULL;

  PetscFunctionBeginUser;
  PetscCall(PetscInitialize(&argc, &argv, (char *)0, NULL));

  /*
    Example
    WORLD:   0 1 2 3 4 5 6 7
    X:         0 1 2 3
    Y:           0 1 2 3 4 5 
  */
  MPI_Comm comm_x, comm_y;

  {
       MPI_Group a, b;
       int ranks[] = {0, 1, 2, 3};
       MPI_Comm_group(MPI_COMM_WORLD, &a);
       MPI_Group_incl(a, 4, ranks, &b);
       MPI_Comm_create_group(MPI_COMM_WORLD, b, 0, &comm_x);
  }


  {
       MPI_Group a, b;
       int ranks[] = {0, 1};
       MPI_Comm_group(MPI_COMM_WORLD, &a);
       MPI_Group_incl(a, 2, ranks, &b);
       MPI_Comm_create_group(MPI_COMM_WORLD, b, 0, &comm_y);
  }

  if (comm_x == MPI_COMM_NULL && comm_y == MPI_COMM_NULL) {
    goto exit;
  }

  if (comm_x != MPI_COMM_NULL) {
    PetscCall(VecCreate(comm_x, &x));
    PetscCall(VecSetSizes(x, PETSC_DECIDE, N));
    PetscCall(VecSetFromOptions(x));
    PetscCall(VecSet(x, one));
    PetscInt low, high;
    VecGetOwnershipRange(x, &low, &high);

    double t = 0.0;
    for (PetscInt index = low; index < high; index++) {
      t = index + (1.0 / index);
      PetscCall(VecSetValues(x, 1, &index, &t, INSERT_VALUES));
    }

    PetscCall(VecAssemblyBegin(x));
    PetscCall(VecAssemblyEnd(x));
  }

  if (comm_y != MPI_COMM_NULL) {
    VecCreate(comm_y, &y);
    VecSetSizes(y, PETSC_DECIDE, N);
    VecSetFromOptions(y);
    VecSet(y, one);
  }

  VecCopyDifferentComm(x, 0, y, 0);
  
  if (comm_x != MPI_COMM_NULL) {
    VecView(x, NULL);
  }

  if (comm_y != MPI_COMM_NULL) {
    VecView(y, NULL);
  }

  if (comm_x != MPI_COMM_NULL) PetscCall(VecDestroy(&x));
  if (comm_y != MPI_COMM_NULL) PetscCall(VecDestroy(&y));

  exit:
  PetscCall(PetscFinalize());
  return 0;
}
