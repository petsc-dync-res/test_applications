/* ------------------------------------------------------------------------

   Source: src/ts/tutorials/ex2.c

   This program solves the PDE

               u * u_xx
         u_t = ---------
               2*(t+1)^2

    on the domain 0 <= x <= 1, with boundary conditions
         u(t,0) = t + 1,  u(t,1) = 2*t + 2,
    and initial condition
         u(0,x) = 1 + x*x.

    The exact solution is:
         u(t,x) = (1 + x*x) * (1 + t)

    Note that since the solution is linear in time and quadratic in x,
    the finite difference scheme actually computes the "exact" solution.

    We use by default the backward Euler method.

  ------------------------------------------------------------------------- */

#include <petscts.h>
#include <petscdm.h>
#include <petscdmda.h>
#include "dyn_petsc.h"

// scons example=DynPETScTS compileMode=release > /dev/null && prterun -n 4 --host n01:2,n02:2,n03:2,n04:2 ./build/DynPETScTS_release

/*
   User-defined application context - contains data needed by the
   application-provided callback routines.
*/
typedef struct {
  MPI_Comm  comm;      /* communicator */
  DM        da;        /* distributed array data structure */
  Vec       localwork; /* local ghosted work vector */
  Vec       u_local;   /* local ghosted approximate solution vector */
  Vec       solution;  /* global exact solution vector */
  PetscInt  m;         /* total number of grid points */
  PetscReal h;         /* mesh width: h = 1/(m-1) */
  PetscBool debug;     /* flag (1 indicates activation of debugging printouts) */
} AppCtx;

/*
   User-defined routines, provided below.
*/
extern PetscErrorCode RHSFunction(TS, PetscReal, Vec, Vec, void *);
extern PetscErrorCode RHSJacobian(TS, PetscReal, Vec, Mat, Mat, void *);
extern PetscErrorCode ExactSolution(PetscReal time);
extern PetscErrorCode SetInitialConditions();
extern PetscErrorCode InitDM();
extern PetscErrorCode InitJacobianMatrix();
extern PetscErrorCode InitTS();

static bool initialized_dm = false;

static TS        ts;                           /* timestepping context */
static Mat       A;                            /* Jacobian matrix data structure */
static Vec       u;                            /* approximate solution vector */
static AppCtx    appctx;                       /* user-defined application context */
static PetscReal dt;                           /* time step size */
static PetscInt  time_steps_max = 5;           /* default max timesteps */
static PetscReal time_total_max = 2.0;         /* default max total time */
static PetscInt  curr_step = 0;
static PetscReal curr_time = .0;

int main(int argc, char **argv) {
   /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
      Initialize program and set problem parameters
      - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

   PetscFunctionBeginUser;

   PetscCall(InitPetsc(&argc, &argv));

   appctx.m = 100;
   appctx.comm = PETSC_COMM_WORLD;

   // Get problem size from cli options
   PetscCall(PetscOptionsGetInt(NULL, NULL, "-m", &appctx.m, NULL));
   PetscCall(PetscOptionsGetInt(NULL, NULL, "-max_steps", &time_steps_max, NULL));

   appctx.h    = 1.0 / (appctx.m - 1.0);
   dt          = appctx.h / 2.0;

   /*
      Call Initialization routines
   */
   PetscCall(InitDM());
   PetscCall(InitJacobianMatrix());
   PetscCall(InitTS());

   /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
      Solve the problem
      - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

   if (!is_dynamic) {
      /*
         Evaluate initial conditions
      */
      PetscCall(SetInitialConditions());
      PetscCall(TSGetStepNumber(ts, &curr_step));
      PetscCall(Monitor());
      PetscCall(PrintNewLine());
   }

   if (world_rank == 0) {
      // Start timer before calculating
      PetscCall(PetscTime(&t_start_global));
   }

  /*
      Run the timestepping solver
   */
   while (curr_step < time_steps_max) {
      
      PetscCall(Step());

      PetscCall(Monitor());

      int pset_op_fail = SendPsetOp();

      if (pset_op_fail) {
         PetscCall(PrintNewLine());
         continue;
      }

      if (terminate) goto clean_up;

      PetscCall(RedistributeObjects());

      PetscCall(AdvancedMonitor());
   }

   PetscCall(PrintResults());

   /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
      Free work space.  All PETSc objects should be destroyed when they
      are no longer needed.
      - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
clean_up:
   PetscCall(CleanUp());

   PetscFunctionReturn(PETSC_SUCCESS);
}
/* --------------------------------------------------------------------- */

PetscErrorCode Step() {
   PetscFunctionBeginUser;

   // measure elapsed time for each iteration
   PetscCall(PetscTime(&t_start));

   PetscCall(TSStep(ts));

   PetscCall(PetscTime(&t_end));

   PetscLogDouble elapsed = t_end - t_start;
   PetscCallMPI(MPI_Reduce(&elapsed, &t_step, 1, MPI_DOUBLE, MPI_MAX, 0, comm_world));

   t_step_sum += t_step;

   PetscCall(TSGetStepNumber(ts, &curr_step));
   PetscCall(TSGetTime(ts, &curr_time));

   PetscFunctionReturn(PETSC_SUCCESS);
}

PetscErrorCode SetInitialConditions() {
  PetscScalar *u_localptr, h = appctx.h, x;
  PetscInt     i, mybase, myend;

  PetscFunctionBeginUser;
  /*
     Determine starting point of each processor's range of
     grid values.
  */
  PetscCall(VecGetOwnershipRange(u, &mybase, &myend));

  /*
    Get a pointer to vector data.
    - For default PETSc vectors, VecGetArray() returns a pointer to
      the data array.  Otherwise, the routine is implementation dependent.
    - You MUST call VecRestoreArray() when you no longer need access to
      the array.
    - Note that the Fortran interface to VecGetArray() differs from the
      C version.  See the users manual for details.
  */
  PetscCall(VecGetArray(u, &u_localptr));

  /*
     We initialize the solution array by simply writing the solution
     directly into the array locations.  Alternatively, we could use
     VecSetValues() or VecSetValuesLocal().
  */
  for (i = mybase; i < myend; i++) {
    x                      = h * (PetscReal)i; /* current location in global grid */
    u_localptr[i - mybase] = 1.0 + x * x;
  }

  /*
     Restore vector
  */
  PetscCall(VecRestoreArray(u, &u_localptr));

  /*
     Print debugging information if desired
  */
  if (appctx.debug) {
    PetscCall(PetscPrintf(appctx.comm, "initial guess vector\n"));
    PetscCall(VecView(u, PETSC_VIEWER_STDOUT_WORLD));
  }

  PetscFunctionReturn(PETSC_SUCCESS);
}

PetscErrorCode ExactSolution(PetscReal t) {
  Vec solution = appctx.solution;
  PetscScalar *s_localptr, h = appctx.h, x;
  PetscInt     i, mybase, myend;

  PetscFunctionBeginUser;
  /*
     Determine starting and ending points of each processor's
     range of grid values
  */
  PetscCall(VecGetOwnershipRange(solution, &mybase, &myend));

  /*
     Get a pointer to vector data.
  */
  PetscCall(VecGetArray(solution, &s_localptr));

  /*
     Simply write the solution directly into the array locations.
     Alternatively, we could use VecSetValues() or VecSetValuesLocal().
  */
  for (i = mybase; i < myend; i++) {
    x                      = h * (PetscReal)i;
    s_localptr[i - mybase] = (t + 1.0) * (1.0 + x * x);
  }

  /*
     Restore vector
  */
  PetscCall(VecRestoreArray(solution, &s_localptr));
  PetscFunctionReturn(PETSC_SUCCESS);
}

PetscErrorCode Monitor() {
  PetscReal en2, en2s;
//   PetscDraw draw;

  PetscFunctionBeginUser;

  if (disable_monitor) PetscFunctionReturn(PETSC_SUCCESS);

  /*
     Compute the exact solution at this timestep
  */
  PetscCall(ExactSolution(curr_time));

  /*
     Compute the 2-norm and max-norm of the error
  */
  PetscCall(VecAXPY(appctx.solution, -1.0, u));
  PetscCall(VecNorm(appctx.solution, NORM_2, &en2));
  en2s = PetscSqrtReal(appctx.h) * en2; /* scale the 2-norm by the grid spacing */

  /*
     PetscPrintf() causes only the first processor in this
     communicator to print the timestep information.
  */
  PetscCall(PetscPrintf(comm_world, "Size: %4d, Step: %3" PetscInt_FMT ", t: %12f, Error: %12g, Elapsed: %8.5f sec",
                           world_size, curr_step, (double) curr_time, (double) en2s, t_step));

  if (!advanced_monitor) PetscCall(PetscPrintf(comm_world, "\n"));

  PetscFunctionReturn(PETSC_SUCCESS);
}


/*
   RHSFunction - User-provided routine that evalues the right-hand-side
   function of the ODE.  This routine is set in the main program by
   calling TSSetRHSFunction().  We compute:
          global_out = F(global_in)

   Input Parameters:
   ts         - timesteping context
   t          - current time
   global_in  - vector containing the current iterate
   ctx        - (optional) user-provided context for function evaluation.
                In this case we use the appctx defined above.

   Output Parameter:
   global_out - vector containing the newly evaluated function
*/
PetscErrorCode RHSFunction(TS ts, PetscReal t, Vec global_in, Vec global_out, void *ctx) {
  AppCtx            *appctx    = (AppCtx *)ctx;     /* user-defined application context */
  DM                 da        = appctx->da;        /* distributed array */
  Vec                local_in  = appctx->u_local;   /* local ghosted input vector */
  Vec                localwork = appctx->localwork; /* local ghosted work vector */
  PetscInt           i, localsize;
  PetscMPIInt        rank, size;
  PetscScalar       *copyptr, sc;
  const PetscScalar *localptr;

  PetscFunctionBeginUser;
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Get ready for local function computations
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  /*
     Scatter ghost points to local vector, using the 2-step process
        DMGlobalToLocalBegin(), DMGlobalToLocalEnd().
     By placing code between these two statements, computations can be
     done while messages are in transition.
  */
  PetscCall(DMGlobalToLocalBegin(da, global_in, INSERT_VALUES, local_in));
  PetscCall(DMGlobalToLocalEnd(da, global_in, INSERT_VALUES, local_in));

  /*
      Access directly the values in our local INPUT work array
  */
  PetscCall(VecGetArrayRead(local_in, &localptr));

  /*
      Access directly the values in our local OUTPUT work array
  */
  PetscCall(VecGetArray(localwork, &copyptr));

  sc = 1.0 / (appctx->h * appctx->h * 2.0 * (1.0 + t) * (1.0 + t));

  /*
      Evaluate our function on the nodes owned by this processor
  */
  PetscCall(VecGetLocalSize(local_in, &localsize));

  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Compute entries for the locally owned part
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

  /*
     Handle boundary conditions: This is done by using the boundary condition
        u(t,boundary) = g(t,boundary)
     for some function g. Now take the derivative with respect to t to obtain
        u_{t}(t,boundary) = g_{t}(t,boundary)

     In our case, u(t,0) = t + 1, so that u_{t}(t,0) = 1
             and  u(t,1) = 2t+ 2, so that u_{t}(t,1) = 2
  */
  PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &rank));
  PetscCallMPI(MPI_Comm_size(PETSC_COMM_WORLD, &size));
  if (rank == 0) copyptr[0] = 1.0;
  if (rank == size - 1) copyptr[localsize - 1] = 2.0;

  /*
     Handle the interior nodes where the PDE is replace by finite
     difference operators.
  */
  for (i = 1; i < localsize - 1; i++)
   copyptr[i] = localptr[i] * sc * (localptr[i + 1] + localptr[i - 1] - 2.0 * localptr[i]);

  /*
     Restore vectors
  */
  PetscCall(VecRestoreArrayRead(local_in, &localptr));
  PetscCall(VecRestoreArray(localwork, &copyptr));

  /*
     Insert values from the local OUTPUT vector into the global
     output vector
  */
  PetscCall(DMLocalToGlobalBegin(da, localwork, INSERT_VALUES, global_out));
  PetscCall(DMLocalToGlobalEnd(da, localwork, INSERT_VALUES, global_out));

  /* Print debugging information if desired */
  if (appctx->debug) {
    PetscCall(PetscPrintf(appctx->comm, "RHS function vector\n"));
    PetscCall(VecView(global_out, PETSC_VIEWER_STDOUT_WORLD));
  }

  PetscFunctionReturn(PETSC_SUCCESS);
}
/* --------------------------------------------------------------------- */

/*
   RHSJacobian - User-provided routine to compute the Jacobian of
   the nonlinear right-hand-side function of the ODE.

   Input Parameters:
   ts - the TS context
   t - current time
   global_in - global input vector
   dummy - optional user-defined context, as set by TSetRHSJacobian()

   Output Parameters:
   AA - Jacobian matrix
   BB - optionally different preconditioning matrix
   str - flag indicating matrix structure

  Notes:
  RHSJacobian computes entries for the locally owned part of the Jacobian.
   - Currently, all PETSc parallel matrix formats are partitioned by
     contiguous chunks of rows across the processors.
   - Each processor needs to insert only elements that it owns
     locally (but any non-local elements will be sent to the
     appropriate processor during matrix assembly).
   - Always specify global row and columns of matrix entries when
     using MatSetValues().
   - Here, we set all entries for a particular row at once.
   - Note that MatSetValues() uses 0-based row and column numbers
     in Fortran as well as in C.
*/
PetscErrorCode RHSJacobian(TS ts, PetscReal t, Vec global_in, Mat AA, Mat BB, void *ctx) {
  AppCtx            *appctx   = (AppCtx *)ctx;   /* user-defined application context */
  Vec                local_in = appctx->u_local; /* local ghosted input vector */
  DM                 da       = appctx->da;      /* distributed array */
  PetscScalar        v[3], sc;
  const PetscScalar *localptr;
  PetscInt           i, mstart, mend, mstarts, mends, idx[3], is;

  PetscFunctionBeginUser;
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Get ready for local Jacobian computations
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  /*
     Scatter ghost points to local vector, using the 2-step process
        DMGlobalToLocalBegin(), DMGlobalToLocalEnd().
     By placing code between these two statements, computations can be
     done while messages are in transition.
  */
  PetscCall(DMGlobalToLocalBegin(da, global_in, INSERT_VALUES, local_in));
  PetscCall(DMGlobalToLocalEnd(da, global_in, INSERT_VALUES, local_in));

  /*
     Get pointer to vector data
  */
  PetscCall(VecGetArrayRead(local_in, &localptr));

  /*
     Get starting and ending locally owned rows of the matrix
  */
  PetscCall(MatGetOwnershipRange(BB, &mstarts, &mends));
  mstart = mstarts;
  mend   = mends;

  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Compute entries for the locally owned part of the Jacobian.
      - Currently, all PETSc parallel matrix formats are partitioned by
        contiguous chunks of rows across the processors.
      - Each processor needs to insert only elements that it owns
        locally (but any non-local elements will be sent to the
        appropriate processor during matrix assembly).
      - Here, we set all entries for a particular row at once.
      - We can set matrix entries either using either
        MatSetValuesLocal() or MatSetValues().
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

  /*
     Set matrix rows corresponding to boundary data
  */
  if (mstart == 0) {
    v[0] = 0.0;
    PetscCall(MatSetValues(BB, 1, &mstart, 1, &mstart, v, INSERT_VALUES));
    mstart++;
  }
  if (mend == appctx->m) {
    mend--;
    v[0] = 0.0;
    PetscCall(MatSetValues(BB, 1, &mend, 1, &mend, v, INSERT_VALUES));
  }

  /*
     Set matrix rows corresponding to interior data.  We construct the
     matrix one row at a time.
  */
  sc = 1.0 / (appctx->h * appctx->h * 2.0 * (1.0 + t) * (1.0 + t));
  for (i = mstart; i < mend; i++) {
    idx[0] = i - 1;
    idx[1] = i;
    idx[2] = i + 1;
    is     = i - mstart + 1;
    v[0]   = sc * localptr[is];
    v[1]   = sc * (localptr[is + 1] + localptr[is - 1] - 4.0 * localptr[is]);
    v[2]   = sc * localptr[is];
    PetscCall(MatSetValues(BB, 1, &i, 3, idx, v, INSERT_VALUES));
  }

  /*
     Restore vector
  */
  PetscCall(VecRestoreArrayRead(local_in, &localptr));

  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Complete the matrix assembly process and set some options
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  /*
     Assemble matrix, using the 2-step process:
       MatAssemblyBegin(), MatAssemblyEnd()
     Computations can be done while messages are in transition
     by placing code between these two statements.
  */
  PetscCall(MatAssemblyBegin(BB, MAT_FINAL_ASSEMBLY));
  PetscCall(MatAssemblyEnd(BB, MAT_FINAL_ASSEMBLY));
  if (BB != AA) {
    PetscCall(MatAssemblyBegin(AA, MAT_FINAL_ASSEMBLY));
    PetscCall(MatAssemblyEnd(AA, MAT_FINAL_ASSEMBLY));
  }

  /*
     Set and option to indicate that we will never add a new nonzero location
     to the matrix. If we do, it will generate an error.
  */
  PetscCall(MatSetOption(BB, MAT_NEW_NONZERO_LOCATION_ERR, PETSC_TRUE));

  PetscFunctionReturn(PETSC_SUCCESS);
}

PetscErrorCode InitDM() {
   Vec _u = u, _u_local = appctx.u_local;

   PetscFunctionBeginUser;

   /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Create vector data structures
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

   /*
      Create distributed array (DMDA) to manage parallel grid and vectors
      and to set up the ghost point communication pattern.  There are M
      total grid values spread equally among all the processors.
   */
   PetscCall(DMDACreate1d(comm_world, DM_BOUNDARY_NONE, appctx.m, 1, 1, NULL, &appctx.da));
   PetscCall(DMSetFromOptions(appctx.da));
   PetscCall(DMSetUp(appctx.da));

   /*
      Extract global and local vectors from DMDA; we use these to store the
      approximate solution.  Then duplicate these for remaining vectors that
      have the same types.
   */
   PetscCall(DMCreateGlobalVector(appctx.da, &u));
   PetscCall(DMCreateLocalVector(appctx.da, &appctx.u_local));

   int should_copy = is_dynamic || initialized_dm;

   /*
      Dynamically allocated processes should copy solution vector 
   */
   if (should_copy) {
      PetscCall(VecCopyDifferentComm(_u, 0, u, 0));
      PetscCall(DMGlobalToLocalBegin(appctx.da, u, INSERT_VALUES, appctx.u_local));
   }

   /*
      Destroy old vectors if exists
   */
   if (_u)       PetscCall(VecDestroy(&_u));
   if (_u_local) PetscCall(VecDestroy(&_u_local));

   /*
      Create local work vector for use in evaluating right-hand-side function;
      create global work vector for storing exact solution.
   */
   PetscCall(VecDuplicate(appctx.u_local, &appctx.localwork));
   PetscCall(VecDuplicate(u, &appctx.solution));

   if (should_copy) PetscCall(DMGlobalToLocalEnd(appctx.da, u, INSERT_VALUES, appctx.u_local));

   initialized_dm = true;

   PetscFunctionReturn(PETSC_SUCCESS);
}

PetscErrorCode InitTS() {
   PetscFunctionBeginUser;

   /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Create timestepping solver context; set callback routine for
     right-hand-side function evaluation.
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

   PetscCall(TSCreate(comm_world, &ts));
   PetscCall(TSSetProblemType(ts, TS_NONLINEAR));
   PetscCall(TSSetRHSFunction(ts, NULL, RHSFunction, &appctx));
   PetscCall(TSSetRHSJacobian(ts, A, A, RHSJacobian, &appctx));

   /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Set solution vector and initial timestep
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

   PetscCall(TSSetTimeStep(ts, dt));
   PetscCall(TSSetSolution(ts, u));

   /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Customize timestepping solver:
       - Set the solution method to be the Backward Euler method.
       - Set timestepping duration info
     Then set runtime options, which can override these defaults.
     For example,
          -ts_max_steps <maxsteps> -ts_max_time <maxtime>
     to override the defaults set by TSSetMaxSteps()/TSSetMaxTime().
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

   PetscCall(TSSetType(ts, TSBEULER));
   PetscCall(TSSetMaxSteps(ts, time_steps_max));
   PetscCall(TSSetMaxTime(ts, time_total_max));
   PetscCall(TSSetExactFinalTime(ts, TS_EXACTFINALTIME_STEPOVER));
   PetscCall(TSSetFromOptions(ts));

   if (is_dynamic || initialized_dm) {

      PetscCallMPI(MPI_Bcast(&curr_step, 1, MPI_INT, 0, comm_world));
      PetscCallMPI(MPI_Bcast(&curr_time, 1, MPI_DOUBLE, 0, comm_world));

      PetscCall(TSSetStepNumber(ts, curr_step));
      PetscCall(TSSetTime(ts, curr_time));
   }

   PetscFunctionReturn(PETSC_SUCCESS);
}

PetscErrorCode InitJacobianMatrix() {
   PetscFunctionBeginUser;

   /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     For nonlinear problems, the user can provide a Jacobian evaluation
     routine (or use a finite differencing approximation).

     Create matrix data structure; set Jacobian evaluation routine.
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

   PetscCall(MatCreate(comm_world, &A));
   PetscCall(MatSetSizes(A, PETSC_DECIDE, PETSC_DECIDE, appctx.m, appctx.m));
   PetscCall(MatSetFromOptions(A));
   PetscCall(MatSetUp(A));

   PetscFunctionReturn(PETSC_SUCCESS);
}

PetscErrorCode RedistributeObjects() {
   PetscFunctionBeginUser;

   if (world_rank == 0) PetscCall(PetscTime(&t_start));

   /*
      Destroy old objects
   */
   PetscCall(TSDestroy(&ts));
   PetscCall(MatDestroy(&A));
   PetscCall(DMDestroy(&appctx.da));
   PetscCall(VecDestroy(&appctx.localwork));
   PetscCall(VecDestroy(&appctx.solution));

   PetscCall(SetCommFromSession());

   MPI_Comm old_comm = PETSC_COMM_WORLD;

   if (next_op == MPI_PSETOP_GROW) PETSC_COMM_WORLD = comm_world;

   PetscCall(InitDM());
   PetscCall(InitJacobianMatrix());
   PetscCall(InitTS());

   if (next_op == MPI_PSETOP_SHRINK) PETSC_COMM_WORLD = comm_world;

   if (world_rank == 0) {
      PetscCall(PetscTime(&t_end));
      t_redis = t_end - t_start;
      t_redis_sum += t_redis;
   }

   PetscCallMPI(MPI_Comm_disconnect(&old_comm));

   PetscFunctionReturn(PETSC_SUCCESS);
}

PetscErrorCode CleanUp() {
   PetscFunctionBeginUser;

   if (terminate) {
      PetscCall(VecCopyDifferentComm(u, 0, NULL, 0));
   }

   PetscCall(TSDestroy(&ts));
   PetscCall(MatDestroy(&A));
   PetscCall(VecDestroy(&u));
   PetscCall(DMDestroy(&appctx.da));
   PetscCall(VecDestroy(&appctx.localwork));
   PetscCall(VecDestroy(&appctx.solution));
   PetscCall(VecDestroy(&appctx.u_local));

   /*
      Always call PetscFinalize() before exiting a program.  This routine
         - finalizes the PETSc libraries as well as MPI
         - provides summary and diagnostic information if certain runtime
            options are chosen (e.g., -log_view).
   */
   PetscCallMPI(MPI_Comm_disconnect(&comm_world));

   PETSC_COMM_WORLD = PETSC_COMM_SELF;
   PetscCall(PetscFinalize());
   
   PetscFunctionReturn(PETSC_SUCCESS);
}