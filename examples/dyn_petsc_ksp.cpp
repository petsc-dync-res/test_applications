/* ------------------------------------------------------------------------

    Source: src/ksp/ksp/tutorials/ex15.c

  ------------------------------------------------------------------------- */

#include <petscksp.h>
#include "dyn_petsc.h"

// scons example=DynPETScKSPIterativeDynamic compileMode=release > /dev/null && prterun -n 2 --host n01:2,n02:2,n03:2,n04:2 ./build/DynPETScKSPIterativeDynamic_release

static Mat A;
static Vec x, b, u;
static KSP ksp;
static PetscInt n = 30, m = 30;     // Size of the linear system
static PetscReal tolerance = 1e-8;  // Tolerance for convergence
static PetscInt iters_per_step = 5; // Max iterations for each step
static KSPConvergedReason reason;
static PetscInt its;

static PetscInt max_iters = 100;    // Maximum number of total iterations
static PetscInt curr_step = 0;

static int initialized_objects = false;

PetscErrorCode InitKSP();
PetscErrorCode InitObjects();
PetscErrorCode InitialObjects();

int main(int argc, char **argv) {
    PetscFunctionBeginUser;

    PetscCall(InitPetsc(&argc, &argv));

    // Get problem size from cli options
    PetscCall(PetscOptionsGetInt(NULL, NULL, "-m", &m, NULL));
    PetscCall(PetscOptionsGetInt(NULL, NULL, "-n", &n, NULL));
    PetscCall(PetscOptionsGetInt(NULL, NULL, "-max_iters", &max_iters, NULL));
    PetscCall(PetscOptionsGetInt(NULL, NULL, "-iters_per_step", &iters_per_step, NULL));
    PetscCall(PetscOptionsGetReal(NULL, NULL, "-tol", &tolerance, NULL));

    PetscCall(InitObjects());
    PetscCall(InitKSP());

    // Start timer before calculating
    if (world_rank == 0) {
        PetscCall(PetscTime(&t_start_global));
    }

    while(max_iters > 0) {

        PetscCall(Step());

        PetscCall(Monitor());

        // Check if converged, then stop
        if (reason > 0) break;

        max_iters -= its; // decrease maximum iterations
        ++curr_step;

        int pset_op_fail = SendPsetOp();

        if (pset_op_fail) {
            PetscCall(PrintNewLine());
            continue;
        }

        if (terminate) goto clean_up;

        PetscCall(RedistributeObjects());

        PetscCall(AdvancedMonitor());
    }

    PetscCall(PrintResults());

clean_up:
    PetscCall(CleanUp());

    PetscFunctionReturn(PETSC_SUCCESS);
}

PetscErrorCode Step() {
    PetscFunctionBeginUser;

    // measure elapsed time for each iteration
    PetscCall(PetscTime(&t_start));

    PetscCall(KSPSolve(ksp, b, x));

    PetscCall(PetscTime(&t_end));

    PetscLogDouble elapsed = t_end - t_start;
    PetscCallMPI(MPI_Reduce(&elapsed, &t_step, 1, MPI_DOUBLE, MPI_MAX, 0, comm_world));

    t_step_sum += t_step;

    // Check convergence
    PetscCall(KSPGetConvergedReason(ksp, &reason));

    PetscCall(KSPGetIterationNumber(ksp, &its));

    PetscFunctionReturn(PETSC_SUCCESS);
}

PetscErrorCode Monitor() {
    PetscReal error_norm;
    Vec tmp;

    PetscFunctionBeginUser;

    if (disable_monitor) PetscFunctionReturn(PETSC_SUCCESS);

    PetscCall(VecDuplicate(x, &tmp));
    PetscCall(VecCopy(x, tmp));

    // tmp = tmp - u
    PetscCall(VecAXPY(tmp, -1.0, u));
    PetscCall(VecNorm(tmp, NORM_2, &error_norm));
    PetscCall(VecDestroy(&tmp));

    PetscCall(PetscPrintf(comm_world, "Size: %4d, Step: %3d, Error: %12g, Its: %2" PetscInt_FMT ", Elapsed: %5f seconds, Conv: %s",
                                                world_size, curr_step, error_norm, its, t_end - t_start, KSPConvergedReasons[reason]));

    if (!advanced_monitor) PetscCall(PetscPrintf(comm_world, "\n"));

    PetscFunctionReturn(PETSC_SUCCESS);
}

PetscErrorCode CleanUp() {
    PetscFunctionBeginUser;

    if (terminate) {
        PetscCall(VecCopyDifferentComm(x, 0, NULL, 0));
    }
    
    PetscCall(KSPDestroy(&ksp));
    PetscCall(MatDestroy(&A));
    PetscCall(VecDestroy(&u));
    PetscCall(VecDestroy(&b));
    PetscCall(VecDestroy(&x));

    PetscCallMPI(MPI_Comm_disconnect(&comm_world));

    PETSC_COMM_WORLD = PETSC_COMM_SELF;
    PetscCall(PetscFinalize());

    PetscFunctionReturn(PETSC_SUCCESS);
}

PetscErrorCode InitKSP() {
    PetscFunctionBeginUser;

     // Set up the linear solver context
    PetscCall(KSPCreate(comm_world, &ksp));
    PetscCall(KSPSetOperators(ksp, A, A));
    PetscCall(KSPSetFromOptions(ksp));

    // Set the desired tolerance and maximum number of iterations
    PetscCall(KSPSetTolerances(ksp, tolerance, PETSC_DEFAULT, PETSC_DEFAULT, iters_per_step));

    // This option is really important, it basically allows us to iteratively improve our solution
    PetscCall(KSPSetInitialGuessNonzero(ksp, PETSC_TRUE));

    PetscFunctionReturn(PETSC_SUCCESS);
}

PetscErrorCode InitObjects() {
    Vec         _x = x;
    PetscInt    i, j, Ii, J, Istart, Iend;
    PetscScalar v;

    PetscFunctionBeginUser;

    PetscCall(MatCreate(comm_world, &A));
    PetscCall(MatSetSizes(A, PETSC_DECIDE, PETSC_DECIDE, m * n, m * n));
    PetscCall(MatSetFromOptions(A));
    PetscCall(MatSetUp(A));

    PetscCall(MatGetOwnershipRange(A, &Istart, &Iend));

    for (Ii = Istart; Ii < Iend; Ii++) {
        v = -1.0;
        i = Ii / n;
        j = Ii - i * n;
        if (i > 0) {
            J = Ii - n;
            PetscCall(MatSetValues(A, 1, &Ii, 1, &J, &v, INSERT_VALUES));
        }
        if (i < m - 1) {
            J = Ii + n;
            PetscCall(MatSetValues(A, 1, &Ii, 1, &J, &v, INSERT_VALUES));
        }
        if (j > 0) {
            J = Ii - 1;
            PetscCall(MatSetValues(A, 1, &Ii, 1, &J, &v, INSERT_VALUES));
        }
        if (j < n - 1) {
            J = Ii + 1;
            PetscCall(MatSetValues(A, 1, &Ii, 1, &J, &v, INSERT_VALUES));
        }
        v = 4.0;
        PetscCall(MatSetValues(A, 1, &Ii, 1, &Ii, &v, INSERT_VALUES));
    }

    PetscCall(MatAssemblyBegin(A, MAT_FINAL_ASSEMBLY));

    PetscCall(MatCreateVecs(A, &x, &b));

    PetscCall(VecDuplicate(x, &u));
    PetscCall(VecSet(u, 1.0));

    int should_copy = is_dynamic || initialized_objects;
    if (should_copy) PetscCall(VecCopyDifferentComm(_x, 0, x, 0));

    if (_x) PetscCall(VecDestroy(&_x));

    PetscCall(MatAssemblyEnd(A, MAT_FINAL_ASSEMBLY));

    // calculate right-hand-side
    PetscCall(MatMult(A, u, b));

    // Broadcast loop variables to all processes
    PetscCallMPI(MPI_Bcast(&max_iters, 1, MPI_INT, 0, comm_world));
    PetscCallMPI(MPI_Bcast(&curr_step, 1, MPI_INT, 0, comm_world));

    initialized_objects = true;

    PetscFunctionReturn(PETSC_SUCCESS);
}

PetscErrorCode RedistributeObjects() {
    MPI_Comm old_comm = PETSC_COMM_WORLD;

    PetscFunctionBeginUser;

    if (world_rank == 0) PetscCall(PetscTime(&t_start));

    PetscCall(KSPDestroy(&ksp));
    PetscCall(MatDestroy(&A));
    PetscCall(VecDestroy(&u));
    PetscCall(VecDestroy(&b));

    PetscCall(SetCommFromSession());

    if (next_op == MPI_PSETOP_GROW) PETSC_COMM_WORLD = comm_world;

    PetscCall(InitObjects());
    PetscCall(InitKSP());

    if (next_op == MPI_PSETOP_SHRINK) PETSC_COMM_WORLD = comm_world;

    if (world_rank == 0) {
        PetscCall(PetscTime(&t_end));
        t_redis = t_end - t_start;
        t_redis_sum += t_redis;
    }

    PetscCallMPI(MPI_Comm_disconnect(&old_comm));

    PetscFunctionReturn(PETSC_SUCCESS);
}