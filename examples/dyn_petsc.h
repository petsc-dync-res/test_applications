#ifndef DYN_PETSC 
#define DYN_PETSC

#include <petsc.h>

static const char world_pset_name[] = "mpi://WORLD";
static const char self_pset_name[] = "mpi://SELF";
static const char BOOL_STRING_TRUE[] = "True";
static const char main_pset_key[] = "main_pset";
static const PetscInt max_int_len = 10;

static MPI_Session session;
static MPI_Info info;
static MPI_Request psetop_req = MPI_REQUEST_NULL;
static char main_pset_name[MPI_MAX_PSET_NAME_LEN];
static bool is_dynamic;

static MPI_Comm comm_world;
static PetscMPIInt world_rank;
static PetscMPIInt world_size;

static int terminate = false;

static PetscLogDouble t_start_global, t_start, t_end;
static PetscLogDouble t_step, t_psetop = .0, t_psetop_query = .0, t_psetop_publish = .0, t_psetop_lookup = .0, t_redis = .0;
static PetscLogDouble t_step_sum = .0, t_psetop_sum = .0, t_redis_sum = .0;

static PetscBool disable_monitor = PETSC_FALSE;
static PetscBool advanced_monitor = PETSC_FALSE;

static char procs_add[max_int_len] = "4";
static char procs_sub[max_int_len] = "4";

enum NextOpAlg {
    SWING,
    GROW_ONLY,
    SHRINK_ONLY,
    RANDOM
};

const char *next_op_alg_names[7] = { "SWING", "GROW_ONLY", "SHRINK_ONLY", "RANDOM", "NextOpAlg", "", NULL };

static NextOpAlg next_op_alg = SWING;
static PetscInt next_op_interval = 1;
static PetscInt iters_since_last_pset_op = 0;
static PetscMPIInt prev_op = MPI_PSETOP_SHRINK;
static PetscMPIInt next_op = MPI_PSETOP_NULL;
static PetscInt rand_seed = -1;

PetscErrorCode DecideNextOp() {
    PetscFunctionBeginUser;

    if (world_rank != 0) {
        PetscFunctionReturn(PETSC_SUCCESS);
    }

    ++iters_since_last_pset_op;

    if (iters_since_last_pset_op != next_op_interval) {
        next_op = MPI_PSETOP_NULL;
        PetscFunctionReturn(PETSC_SUCCESS);
    }

    switch (next_op_alg) {
        case SWING:
        next_op = (prev_op == MPI_PSETOP_GROW ? MPI_PSETOP_SHRINK : MPI_PSETOP_GROW);
        prev_op = next_op;
        break;

        case GROW_ONLY:
        next_op = MPI_PSETOP_GROW;
        break;

        case SHRINK_ONLY:
        next_op = MPI_PSETOP_SHRINK;
        break;

        case RANDOM:
        next_op = (random() % 2 ? MPI_PSETOP_SHRINK : MPI_PSETOP_GROW);
        prev_op = next_op;
        break;

        default:
        next_op = MPI_PSETOP_NULL;
    }

    iters_since_last_pset_op = 0;

    PetscFunctionReturn(PETSC_SUCCESS);
}

PetscErrorCode FreeStringArray(char **array, int size) {
    PetscFunctionBeginUser;

    for (int i = 0; i < size; i++) {
        free(array[i]);
    }
    free(array);

    PetscFunctionReturn(PETSC_SUCCESS);
}

#define DEBUG_LOOP \
    int i = 0; \
    while (i == 0) \
        sleep(1);

PetscErrorCode RedistributeObjects();
PetscErrorCode CleanUp();
PetscErrorCode Step();
PetscErrorCode Monitor();

PetscErrorCode SetCommFromSession() {
    MPI_Group group;

    PetscFunctionBeginUser;

    PetscCallMPI(MPI_Group_from_session_pset(session, main_pset_name, &group));
    PetscCallMPI(MPI_Comm_create_from_group(group, "dyn_procs", MPI_INFO_NULL, MPI_ERRORS_ARE_FATAL, &comm_world));

    PetscCallMPI(MPI_Comm_rank(comm_world, &world_rank));
    PetscCallMPI(MPI_Comm_size(comm_world, &world_size));

    PetscCallMPI(MPI_Group_free(&group));

    PetscFunctionReturn(PETSC_SUCCESS);
}

PetscErrorCode GetInfoFromSession() {
    const int nkey = 1;
    char *keys[nkey] = {
        (char*) main_pset_key
    };
    PetscLogDouble start, end;
    PetscFunctionBeginUser;

    int key_exists;

    PetscCall(PetscTime(&start));

    PetscCallMPI(MPI_Session_get_pset_data(session, (char*) world_pset_name, (char*) world_pset_name, keys, nkey, true, &info));

    PetscCall(PetscTime(&end));
    t_psetop_lookup = end - start;

    PetscCallMPI(MPI_Info_get(info, main_pset_key, MPI_MAX_PSET_NAME_LEN, main_pset_name, &key_exists));
    PetscCallMPI(MPI_Info_free(&info));

    if (!key_exists) {
        printf("main pset key does not exist on pset info\n");
        PetscCallMPI(MPI_Abort(comm_world, PETSC_ERR_RETURN));
    }

    PetscFunctionReturn(PETSC_SUCCESS);
}

PetscErrorCode InitPetsc(int *argc, char ***argv) {
    PetscFunctionBeginUser;

    PetscCall(PetscInitializeSession(NULL, argc, argv, NULL, NULL));
    session    = PETSC_SESSION;
    is_dynamic = PetscIsDynamic;

    if (is_dynamic) {
        PetscCall(GetInfoFromSession());
    } else {
        // initially main pset is world
        strcpy(main_pset_name, world_pset_name);
    }

    PetscCall(SetCommFromSession());
    PETSC_COMM_WORLD = comm_world;

    PetscCall(PetscOptionsHasName(NULL, NULL, "-disable_monitor", &disable_monitor));
    PetscCall(PetscOptionsHasName(NULL, NULL, "-advanced_monitor", &advanced_monitor));

    PetscCall(PetscOptionsGetString(NULL, NULL, "-procs_add", procs_add, max_int_len, NULL));
    PetscCall(PetscOptionsGetString(NULL, NULL, "-procs_sub", procs_sub, max_int_len, NULL));
    
    PetscCall(PetscOptionsGetInt(NULL, NULL, "-next_op_interval", &next_op_interval, NULL));
    PetscCall(PetscOptionsGetEnum(NULL, NULL, "-next_op_alg", next_op_alg_names, (PetscEnum *) &next_op_alg, NULL));
    PetscCall(PetscOptionsGetInt(NULL, NULL, "-random_seed", &rand_seed, NULL));

    if (rand_seed > 0) srand(rand_seed);

    if (is_dynamic && advanced_monitor) {
        PetscLogDouble dummy;
        PetscCallMPI(MPI_Reduce(&t_psetop_lookup, NULL, 1, MPI_DOUBLE, MPI_MAX, 0, comm_world));
    }

    PetscFunctionReturn(PETSC_SUCCESS);
}

PetscErrorCode SendPsetOp_private() {
    PetscLogDouble start, end;
    PetscFunctionBeginUser;

    int procs_sub_int = atoi(procs_sub);

    // primary process tries to send pset op
    if (
        world_rank == 0 &&
        psetop_req == MPI_REQUEST_NULL &&
        next_op != MPI_PSETOP_NULL &&
        (next_op != MPI_PSETOP_SHRINK || procs_sub_int < world_size)
    ) {
        int op = next_op;

        PetscCallMPI(MPI_Info_create(&info));

        if (op == MPI_PSETOP_SHRINK) {
            PetscCallMPI(MPI_Info_set(info, "mpi_num_procs_sub", procs_sub));
        } else {
            PetscCallMPI(MPI_Info_set(info, "mpi_num_procs_add", procs_sub));
        }

        const int ninput = 1;
        char *input_psets[ninput] = {
            (char*) main_pset_name
        };

        char **out_psets;
        int nout = 0;

        // send operation request
        PetscCallMPI(MPI_Session_dyn_v2a_psetop_nb(session, &op, input_psets, ninput, &out_psets, &nout, info, &psetop_req));
        PetscCallMPI(MPI_Info_free(&info));
    }

    // Query information from all processes and adjust main pset and PETSc resources accordingly
    int op = MPI_PSETOP_NULL;
    char **out_psets;
    int nout = 0;
    if (advanced_monitor) PetscCall(PetscTime(&start));
    PetscCallMPI(MPI_Session_dyn_v2a_query_psetop(session, main_pset_name, main_pset_name, &op, &out_psets, &nout));
    if (advanced_monitor) {
        PetscCall(PetscTime(&end));
        PetscLogDouble elapsed = end - start;
        PetscCallMPI(MPI_Reduce(&elapsed, &t_psetop_query, 1, MPI_DOUBLE, MPI_MAX, 0, comm_world));
    }

    if (op == MPI_PSETOP_NULL || 0 == strcmp(main_pset_name, out_psets[1])) {
        // PetscCall(PetscPrintf(comm_world, "Requested resource was not available. Will continue loop...\n"));
        PetscFunctionReturn(PETSC_ERR_RETURN);
    }

    next_op = op; // for querying processes

    char old_main_pset[MPI_MAX_PSET_NAME_LEN];
    strcpy(old_main_pset, main_pset_name);
    strcpy(main_pset_name, out_psets[1]);

    if (world_rank == 0) {

        /* Publish the name of the new main PSet on the delta Pset */
        PetscCallMPI(MPI_Info_create(&info));

        // first out
        PetscCallMPI(MPI_Info_set(info, main_pset_key, out_psets[1]));

        if (advanced_monitor) PetscCall(PetscTime(&start));
        PetscCallMPI(MPI_Session_set_pset_data(session, out_psets[0], info));

        if (advanced_monitor) {
            PetscCall(PetscTime(&end));
            t_psetop_publish = end - start;
        }
        
        PetscCallMPI(MPI_Info_free(&info));
        PetscCallMPI(MPI_Request_free(&psetop_req));
    }

    PetscCall(FreeStringArray(out_psets, nout));

    // check if process is included in delta pset of shrink, if not terminate it
    if (op == MPI_PSETOP_SHRINK) {
        int key_exists;
        char bool_str[6];
        PetscCallMPI(MPI_Session_get_pset_info(session, main_pset_name, &info));
        PetscCallMPI(MPI_Info_get(info, "mpi_included", 6, bool_str, &key_exists));
        PetscCallMPI(MPI_Info_free(&info));

        if (!key_exists || 0 != strcmp(bool_str, BOOL_STRING_TRUE)) {
            terminate = true;
        }
    }

    // primary process finalizes psetop
    if (world_rank == 0) {
        PetscCallMPI(MPI_Session_dyn_finalize_psetop(session, old_main_pset));
    }

    PetscFunctionReturn(PETSC_SUCCESS);
}

PetscErrorCode SendPsetOp() {
    PetscFunctionBeginUser;

    PetscCall(DecideNextOp());

    if (world_rank == 0) PetscCall(PetscTime(&t_start));

    int pset_op_fail = SendPsetOp_private();

    if (world_rank == 0) {
        PetscCall(PetscTime(&t_end));
        t_psetop = t_end - t_start;
        t_psetop_sum += t_psetop;
    }

    PetscFunctionReturn(pset_op_fail);
}

PetscErrorCode AdvancedMonitor() {
    PetscFunctionBeginUser;

    if (!advanced_monitor || disable_monitor) PetscFunctionReturn(PETSC_SUCCESS);

    t_psetop_lookup = 0.0; // only new processes should send it
    PetscLogDouble max_lookup = .0;
    if (next_op == MPI_PSETOP_GROW) {
        PetscCallMPI(MPI_Reduce(&t_psetop_lookup, &max_lookup, 1, MPI_DOUBLE, MPI_MAX, 0, comm_world));
    }

    const char *op = (next_op == MPI_PSETOP_GROW ? "GROW" : "SHRINK");
    PetscCall(PetscPrintf(comm_world, ", t_redis: %8.5f, t_psetop: %8.5f, t_psetop_query: %8.5f, t_psetop_publish: %8.5f, t_psetop_lookup: %8.5f, op: %6s\n",
                            t_redis, t_psetop, t_psetop_query, t_psetop_publish, max_lookup, op));

    PetscFunctionReturn(PETSC_SUCCESS);
}

PetscErrorCode PrintResults() {
    PetscFunctionBeginUser;

    // Calculate elapsed time for each process
    PetscCall(PetscTime(&t_end));
    PetscLogDouble t_total = (t_end - t_start_global);

    PetscCall(PetscPrintf(comm_world, "\n\n"));
    PetscCall(PetscPrintf(comm_world, "Total time             %8.5f sec\n\n", t_total));

    PetscCall(PetscPrintf(comm_world, "Sum of steps           %8.5f sec %5.1f%%\n", t_step_sum, t_step_sum / t_total * 100.0));

    PetscCall(PetscPrintf(comm_world, "Comm overhead total    %8.5f sec %5.1f%%\n\n", t_total - t_step_sum, (1.0 - t_step_sum / t_total) * 100.0));

    PetscCall(PetscPrintf(comm_world, "Sum of psetop          %8.5f sec %5.1f%%\n", t_psetop_sum, t_psetop_sum / t_total * 100.0));
    PetscCall(PetscPrintf(comm_world, "Sum of redis           %8.5f sec %5.1f%%\n\n", t_redis_sum, t_redis_sum / t_total * 100.0));

    PetscFunctionReturn(PETSC_SUCCESS);
}

PetscErrorCode PrintNewLine() {
    PetscFunctionBeginUser;

    if (!disable_monitor && advanced_monitor) PetscCall(PetscPrintf(PETSC_COMM_WORLD, "\n"));

    PetscFunctionReturn(PETSC_SUCCESS);
}

#endif