static char help[] = "";

// scons example=DynPETScMatChangeComm compileMode=release > /dev/null && prterun -n 8 --host n01:8 ./build/DynPETScMatChangeComm_release

#include <petscmat.h>

int main(int argc, char **argv) {
  PetscMPIInt rank;
  PetscInt M = 10, N = 10;
  Mat A = NULL, B = NULL;

  PetscFunctionBeginUser;
  PetscCall(PetscInitialize(&argc, &argv, (char *)0, help));

  /*
    Example
    WORLD:   0 1 2 3 4 5 6 7
    X:         0 1 2 3
    Y:           0 1 2 3 4 5 
  */

  MPI_Comm comm_A, comm_B;

  {
       MPI_Group a, b;
       int ranks[] = {1, 2, 3, 4};
       MPI_Comm_group(MPI_COMM_WORLD, &a);
       MPI_Group_incl(a, 1, ranks, &b);
       MPI_Comm_create_group(MPI_COMM_WORLD, b, 0, &comm_A);
  }


  {
       MPI_Group a, b;
       int ranks[] = {1, 2, 3, 4, 5, 6, 7};
       MPI_Comm_group(MPI_COMM_WORLD, &a);
       MPI_Group_incl(a, 2, ranks, &b);
       MPI_Comm_create_group(MPI_COMM_WORLD, b, 0, &comm_B);
  }

  if (comm_A == MPI_COMM_NULL && comm_B == MPI_COMM_NULL) {
    goto exit;
  }

  if (comm_A != MPI_COMM_NULL) {

    PetscCall(MatCreate(comm_A, &A));
    PetscCall(MatSetSizes(A, PETSC_DECIDE, PETSC_DECIDE, M, N));
    PetscCall(MatSetFromOptions(A));
    PetscCall(MatZeroEntries(A));

    PetscInt rstart, rend, cstart, cend;
    PetscCall(MatGetOwnershipRange(A, &rstart, &rend));
    PetscCall(MatGetOwnershipRangeColumn(A, &cstart, &cend));


    for (PetscInt i = rstart; i < rend; i++)
    {
      for (PetscInt j = 0; j < N; j++)
      {
          MatSetValue(A, i, j, i * N + j, INSERT_VALUES);
      }
      
    }

    MatAssemblyBegin(A, MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(A, MAT_FINAL_ASSEMBLY);
  }

  if (comm_B != MPI_COMM_NULL) {
    PetscCall(MatCreate(comm_B, &B));
    PetscCall(MatSetSizes(B, PETSC_DECIDE, PETSC_DECIDE, M, N));
    PetscCall(MatSetFromOptions(B));
  }

  MatCopyDifferentComm(A, 1, B, 2);

  // if (comm_A != MPI_COMM_NULL) {
  //   MatView(A, NULL);
  // }

  if (comm_B != MPI_COMM_NULL) {
    MatView(B, NULL);
  }

  exit:
  PetscCall(PetscFinalize());
  return 0;
}
