#!/bin/bash
its=10
n_procs=2
m=100
max_steps=20
timestamp=$(date +%s)

file="ts_${its}_${n_procs}_${m}_${max_steps}_${timestamp}.txt"
echo "" > $file

echo "RUNNING PETSc Test TS" | tee -a $file

# Function to handle termination
cleanup() {
    echo "Script terminated." | tee -a $file
    exit 1
}

# Trap Ctrl+C signal and call the cleanup function
trap cleanup INT

for ((i=1; i<=its; i++))
do
    echo "Running iteration $i" | tee -a $file
    prterun -n $n_procs --host n01:2,n02:2,n03:2,n04:2 ./build/DynPETScTS_release -m $m -max_steps $max_steps -disable_monitor | tee -a $file
done