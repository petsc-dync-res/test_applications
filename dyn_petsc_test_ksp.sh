#!/bin/bash
its=10
n_procs=2
m=10
n=10
bs=2
iters_per_step=5
timestamp=$(date +%s)

file="ksp_${its}_${n_procs}_${m}_${n}_${bs}_${iters_per_step}_${timestamp}.txt"
echo "" > $file

echo "RUNNING PETSc Test KSPSolver" | tee -a $file

# Function to handle termination
cleanup() {
    echo "Script terminated." | tee -a $file
    exit 1
}

# Trap Ctrl+C signal and call the cleanup function
trap cleanup INT

for ((i=1; i<=its; i++))
do
    echo "Running iteration $i" | tee -a $file
    prterun -n $n_procs --host n01:2,n02:2,n03:2,n04:2 ./build/DynPETScKSPIterativeDynamic_release -m $m -n $n -bs $bs -iters_per_step $iters_per_step | tee -a $file
done